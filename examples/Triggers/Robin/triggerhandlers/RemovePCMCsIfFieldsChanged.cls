public class RemovePCMCsIfFieldsChanged implements Triggers.Handler {
    public void handle() {
        Set<String> fieldsToCheck = getFieldsToCheck();

        if(!fieldsToCheck.isEmpty()){
            Set<Id> changedQuoteLinesItemIds = new Set<Id>();
            Set<Id> quoteIdsToUpdate = new Set<Id>();

            for (QuoteLineItem newLine : (List<QuoteLineItem>)Trigger.new) {
                QuoteLineItem oldLine = (QuoteLineItem)Trigger.oldMap.get(newLine.Id);

                for(String field : fieldsToCheck){
                    if(newLine.get(field) != oldLine.get(field)){
                        changedQuoteLinesItemIds.add(newLine.Id);
                        quoteIdsToUpdate.add(newLine.QuoteId);
                        break;
                    }
                }
            }
            if (!changedQuoteLinesItemIds.isEmpty()) {
                //Delete Price Conditions and Metal Classifciations
                deletePriceConditionsAndMetalClassifications(changedQuoteLinesItemIds);

                //Update the quotes simulation status
                changeQuoteSimulationStatusToResimulate(quoteIdsToUpdate);
            }
        }
    }
    public Set<String> getFieldsToCheck(){
        List<String> fieldsToCheck = new Utils.Pluck().strings(
                zCRM_Lightning_Layout_Column__c.zCRM_SaveField__c,
                new List<zCRM_Lightning_Layout_Column__c>([SELECT zCRM_SaveField__c FROM zCRM_Lightning_Layout_Column__c WHERE zCRM_Resets_PC_MC__c = TRUE]));
        return new Set<String>(fieldsToCheck);
    }
    public void deletePriceConditionsAndMetalClassifications(Set<Id> quoteLineItemIds){
        List<SObject> toDelete = new List<SObject>();
        toDelete.addAll((List<SObject>)[SELECT Id FROM zCRM_Quote_Line_Price_Condition__c WHERE zCRM_Quote_Line_Item__c IN :quoteLineItemIds]);
        toDelete.addAll((List<SObject>)[SELECT Id FROM zCRM_Quote_Line_Metal_Classification__c WHERE zCRM_Quote_Line_Item__c IN :quoteLineItemIds]);
        
        for(List<SObject> batch : Utils.createBatches(toDelete)){
            delete batch;
        }
    }
    public void changeQuoteSimulationStatusToResimulate(Set<Id> quoteIds){
        List<Quote> quotes = new List<Quote>([SELECT Id, zCRM_Simulation_Status__c FROM Quote WHERE Id IN :quoteIds]);
        for(Quote q : quotes) q.zCRM_Simulation_Status__c = 'To Resimulate';
        
        for(List<SObject> batch : Utils.createBatches(quotes)){
            update batch;
        }
    }
}