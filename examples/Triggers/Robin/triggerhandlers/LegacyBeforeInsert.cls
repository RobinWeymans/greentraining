public class LegacyBeforeInsert implements Triggers.Handler {
    public void handle() {
        for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.new) {
            if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS) {
                item.JobId__c = QuoteLineItemQueueableJob.getJobId();
                item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
            }
        }
    }
}