public class AutoFillExternalProductNote implements Triggers.Handler {

    public void handle() {
        Map<Id, Quote> parentQuotes = new Map<Id, Quote>([SELECT zCRM_Language__c, AccountId FROM Quote WHERE Id IN :new Utils.Pluck().uniqueIds(QuoteLineItem.QuoteId, (List<QuoteLineItem>)Trigger.new)]);
        Set<Id> productIds = new Utils.Pluck().uniqueIds(QuoteLineItem.Product2Id, (List<QuoteLineItem>)Trigger.new);
        Map<Id, Map<String, String>> productSalesTexts = getProductSalesTexts(productIds);
        Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts = getProductUtilizationSalesTexts(productIds);

        for(QuoteLineItem qli : (List<QuoteLineItem>)Trigger.new){
            //Only fill in the external product note if it's empty and you have a value to fill it in.
            if(String.isBlank(qli.zCRM_External_Product_Note__c)){
                String language = parentQuotes.get(qli.QuoteId).zCRM_Language__c;
                Id accountId = parentQuotes.get(qli.QuoteId).AccountId;

                qli.zCRM_External_Product_Note__c = getExternalProductNote(productSalesTexts, productUtilizationSalesTexts, qli.Product2Id, accountId, language);
            }
        }

    }

    public String getExternalProductNote(Map<Id, Map<String, String>> productSalesTexts, Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts, Id productId, Id accountId, String language){
        String pst = '';
        if(productSalesTexts.containsKey(productId)){
            if(productSalesTexts.get(productId).containsKey(language)){
                pst = productSalesTexts.get(productId).get(language);
            }else if(productSalesTexts.get(productId).containsKey('EN')){
                pst = productSalesTexts.get(productId).get('EN');
            }
        }

        String pust = '';
        if(productUtilizationSalesTexts.containsKey(productId)){
            if(productUtilizationSalesTexts.get(productId).containsKey(accountId)){
                if(productUtilizationSalesTexts.get(productId).get(accountId).containsKey(language)){
                    pust = productUtilizationSalesTexts.get(productId).get(accountId).get(language);
                }else if(productUtilizationSalesTexts.get(productId).get(accountId).containsKey('EN')){
                    pust = productUtilizationSalesTexts.get(productId).get(accountId).get('EN');
                }
            }
        }

        return combinePstPust(pst, pust);
    }

    private String combinePstPust(String pst, String pust){
        String externalProductNote;
        if(String.isNotBlank(pst) && String.isNotBlank(pust)){
            externalProductNote = pst + '\n' + pust;
        }else if(String.isBlank(pst)){
            externalProductNote = pust;
        }else if(String.isBlank(pust)){
            externalProductNote = pst;
        }
        return externalProductNote;
    }

    // Accessed with ProductId then Language
    public Map<Id, Map<String, String>> getProductSalesTexts(Set<Id> productIds){
        Map<Id, Map<String, String>> dictionary = new Map<Id, Map<String, String>>();
        for(zCRM_Product_Sales_Text__c pst : [SELECT Id, zCRM_Language__c, zCRM_SAP_Sales_Text__c, zCRM_Product__c FROM zCRM_Product_Sales_Text__c WHERE zCRM_Product__c IN :productIds]){
            if(!dictionary.containsKey(pst.zCRM_Product__c)){
                dictionary.put(pst.zCRM_Product__c, new Map<String, String>());
            }

            dictionary.get(pst.zCRM_Product__c).put(pst.zCRM_Language__c, pst.zCRM_SAP_Sales_Text__c);
        }
        return dictionary;
    }

    //Accessed with ProductId then AccountId then Language
    public Map<Id, Map<Id, Map<String, String>>> getProductUtilizationSalesTexts(Set<Id> productIds){
        Map<Id, Map<Id, Map<String, String>>> dictionary = new Map<Id, Map<Id, Map<String, String>>>();
        for(zCRM_Product_Utilization_Sales_Text__c pust : [SELECT Id, zCRM_Language__c, zCRM_SAP_Sales_Text__c, zCRM_Product_Utilization__r.zCRM_Product__c, zCRM_Product_Utilization__r.zCRM_Account__c FROM zCRM_Product_Utilization_Sales_Text__c WHERE zCRM_Product_Utilization__r.zCRM_Product__c IN :productIds]){
            if(!dictionary.containsKey(pust.zCRM_Product_Utilization__r.zCRM_Product__c)){
                dictionary.put(pust.zCRM_Product_Utilization__r.zCRM_Product__c, new Map<Id, Map<String, String>>());
            }

            if(!dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).containsKey(pust.zCRM_Product_Utilization__r.zCRM_Account__c)){
                dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).put(pust.zCRM_Product_Utilization__r.zCRM_Account__c, new Map<String, String>());
            }

            dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).get(pust.zCRM_Product_Utilization__r.zCRM_Account__c).put(pust.zCRM_Language__c, pust.zCRM_SAP_Sales_Text__c);
        }
        return dictionary;
    }

    public String buildExternalProductNote(QuoteLineItem quoteLineItem){
        Map<Id, Map<String, String>> productSalesTexts = getProductSalesTexts(new Set<Id>{quoteLineItem.Product2Id});
        Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts = getProductUtilizationSalesTexts(new Set<Id>{quoteLineItem.Product2Id});
        Quote parent = [SELECT zCRM_Language__c, AccountId FROM Quote WHERE Id = :quoteLineItem.QuoteId ];

        return getExternalProductNote(productSalesTexts, productUtilizationSalesTexts, quoteLineItem.Product2Id, parent.AccountId, parent.zCRM_Language__c);
    }

}