public class LegacyBeforeUpdate implements Triggers.Handler {
    public final Map<Id, Double> mapPrevious_NonMetal = new Map<Id, Double>();
    public final Map<Id, Double> mapPrevious_Quantity  = new Map<Id, Double>();
    
    public void handle() {
        for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.new) {
            QuoteLineItem oldItem = (QuoteLineItem) Trigger.oldMap.get(item.Id);

            Double previousNonMetal = mapPrevious_NonMetal.containsKey(item.Id)
                    ? mapPrevious_NonMetal.get(item.Id)
                    : oldItem.zCRM_Non_Metal_Value__c;
            
            Double previousQuantity = mapPrevious_Quantity.containsKey(item.Id)
                    ? mapPrevious_Quantity.get(item.Id)
                    : oldItem.Quantity;

            if (item.zCRM_Non_Metal_Value__c != previousNonMetal) {
                mapPrevious_NonMetal.put(item.Id, item.zCRM_Non_Metal_Value__c);
                if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS
                        && !(isNullOrZero(item.zCRM_Non_Metal_Value__c) && isNullOrZero(previousNonMetal))) {
                    item.JobId__c = QuoteLineItemQueueableJob.getJobId();
                    item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
                }
            }
            
            if (item.Quantity != previousQuantity) {
                mapPrevious_Quantity.put(item.Id, item.Quantity);
                if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS
                        && !(isNullOrZero(item.Quantity) && isNullOrZero(previousQuantity))) {
                    item.JobId__c = QuoteLineItemQueueableJob.getJobId();
                    item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
                }
            }
        }
    }
    private Boolean isNullOrZero(Decimal qli){
        return (qli == 0.0 || qli == null);
    }
}