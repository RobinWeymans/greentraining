public class RollupLineComments implements Triggers.Handler {
    public String buildInternalComments(Quote q){
        return String.isBlank(q.zCRM_Internal_Comments__c)
                ? ''
                : q.zCRM_Internal_Comments__c.substringBefore('\nLINE COMMENTS');
    }
    public String buildInternalComments(QuoteLineItem qli){
        return String.isBlank(qli.zCRM_Internal_Comments__c) || qli.zCRM_Won_Lost__c != 'Won'
                ? ''
                : qli.zCRM_Product_Name__c + '-' + qli.zCRM_Internal_Comments__c;
    }

    public void handle() {
        //Filter out the QuoteLineItems where the zCRM_Internal_Comments__c field was changed
        List<QuoteLineItem> changedQuoteLineItems = new Utils.Filter().changedField(QuoteLineItem.zCRM_Internal_Comments__c, Trigger.new, Trigger.old);
        if(changedQuoteLineItems.isEmpty()) return;

//            Still in the codebase to demonstrate how above filter can be used.
//            List<QuoteLineItem> filtered = new List<QuoteLineItem>();
//            for(QuoteLineItem qli : (List<QuoteLineItem>)Trigger.new){
//                if(qli.zCRM_Internal_Comments__c != ((QuoteLineItem)Trigger.oldMap.get(qli.Id)).zCRM_Internal_Comments__c)
//                    filtered.add(qli);
//            }


        Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId = new Utils.GroupBy().ids(QuoteLineItem.QuoteId, changedQuoteLineItems);

        List<Quote> quotesToUpdate = new List<Quote>();

        for (Quote qte : [SELECT Id, zCRM_Internal_Comments__c FROM Quote WHERE Id IN :quoteLineItemsByQuoteId.keySet() AND Status = 'In The Bag']) {
            String quoteInternalComments = buildInternalComments(qte);

            List<String> qliInternalComments = new List<String>();
            for(QuoteLineItem quoteLineItem : quoteLineItemsByQuoteId.get(qte.Id)){
                String qliInternalComment = buildInternalComments(quoteLineItem);

                if(!String.isBlank(qliInternalComment)) //null or ''
                    qliInternalComments.add(qliInternalComment);
            }

            quoteInternalComments = (String.isBlank(quoteInternalComments) ? '' : quoteInternalComments + '\n') +
                (qliInternalComments.isEmpty() ? '' : 'LINE COMMENTS:\n' +
                        String.join(qliInternalComments, '\n'));


            if(!qte.zCRM_Internal_Comments__c.equals(quoteInternalComments)){
                qte.zCRM_Internal_Comments__c = quoteInternalComments;
                quotesToUpdate.add(qte);
            }

        }

        if(!quotesToUpdate.isEmpty())
            update quotesToUpdate;
    }
}