public class LegacyBeforeDelete implements Triggers.Handler {
    public void handle() {
        List<QuoteLineItem> qts = new List<QuoteLineItem>();

        for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.old) {
            if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS) {
                qts.add(new QuoteLineItem(
                        Id = item.Id,
                        JobId__c = QuoteLineItemQueueableJob.getJobId(),
                        JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name()));
            }
        }
        update qts;
    }
}