trigger QuoteLineItemTrigger on QuoteLineItem (before update, before insert, before delete ) {
    new Triggers()
            //Pre release 2
            .bind(Triggers.Evt.beforeupdate, new LegacyBeforeUpdate())
            .bind(Triggers.Evt.beforeinsert, new LegacyBeforeInsert())
            .bind(Triggers.Evt.beforedelete, new LegacyBeforeDelete())

            //Release 2
            .bind(Triggers.Evt.beforeinsert, new AutoFillExternalProductNote())
            .bind(Triggers.Evt.beforeupdate, new RemovePCMCsIfFieldsChanged())
		    .bind(Triggers.Evt.beforeupdate, new RollupLineComments())
		    
            .manage();
}