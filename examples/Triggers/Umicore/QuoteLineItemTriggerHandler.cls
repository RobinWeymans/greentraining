public class QuoteLineItemTriggerHandler {

	//TODO: Find a suitable name that describes what it does
    public class LegacyBeforeDelete implements Triggers.Handler {
        public void handle() {
	        List<QuoteLineItem> qts = new List<QuoteLineItem>();
	
	        for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.old) {
		        if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS) {
			        qts.add(new QuoteLineItem(
					        Id = item.Id,
					        JobId__c = QuoteLineItemQueueableJob.getJobId(),
					        JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name()));
		        }
	        }
	        update qts;
        }
    }
	public class LegacyBeforeUpdate implements Triggers.Handler {
		public final Map<Id, Double> mapPrevious_NonMetal = new Map<Id, Double>();
		public final Map<Id, Double> mapPrevious_Quantity  = new Map<Id, Double>();
		
		public void handle() {
			for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.new) {
				QuoteLineItem oldItem = (QuoteLineItem) Trigger.oldMap.get(item.Id);

				Double previousNonMetal = mapPrevious_NonMetal.containsKey(item.Id)
						? mapPrevious_NonMetal.get(item.Id)
						: oldItem.zCRM_Non_Metal_Value__c;
				
				Double previousQuantity = mapPrevious_Quantity.containsKey(item.Id)
						? mapPrevious_Quantity.get(item.Id)
						: oldItem.Quantity;

				if (item.zCRM_Non_Metal_Value__c != previousNonMetal) {
					mapPrevious_NonMetal.put(item.Id, item.zCRM_Non_Metal_Value__c);
					if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS
							&& !(isNullOrZero(item.zCRM_Non_Metal_Value__c) && isNullOrZero(previousNonMetal))) {
						item.JobId__c = QuoteLineItemQueueableJob.getJobId();
						item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
					}
				}
				
				if (item.Quantity != previousQuantity) {
					mapPrevious_Quantity.put(item.Id, item.Quantity);
					if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS
							&& !(isNullOrZero(item.Quantity) && isNullOrZero(previousQuantity))) {
						item.JobId__c = QuoteLineItemQueueableJob.getJobId();
						item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
					}
				}
			}
		}
		private Boolean isNullOrZero(Decimal qli){
			return (qli == 0.0 || qli == null);
		}
	}
	public class LegacyBeforeInsert implements Triggers.Handler {
		public void handle() {
			for (QuoteLineItem item : (List<QuoteLineItem>) Trigger.new) {
				if(!SystemConstants.CALCULATE_ZCRM_APPROVAL_VALUE_SAVE_AND_SIMULATE_IN_PROGRESS) {
					item.JobId__c = QuoteLineItemQueueableJob.getJobId();
					item.JobContext__c = QuoteLineItemQueueableJob.JobContext.CALCULATE_ZCRM_APPROVAL_VALUE.name();
				}
			}
		}
	}
    
    public class AutoFillExternalProductNote implements Triggers.Handler {

        public void handle() {
            Map<Id, Quote> parentQuotes = new Map<Id, Quote>([SELECT zCRM_Language__c, AccountId FROM Quote WHERE Id IN :new Utils.Pluck().uniqueIds(QuoteLineItem.QuoteId, (List<QuoteLineItem>)Trigger.new)]);
            Set<Id> productIds = new Utils.Pluck().uniqueIds(QuoteLineItem.Product2Id, (List<QuoteLineItem>)Trigger.new);
            Map<Id, Map<String, String>> productSalesTexts = getProductSalesTexts(productIds);
            Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts = getProductUtilizationSalesTexts(productIds);

            for(QuoteLineItem qli : (List<QuoteLineItem>)Trigger.new){
                //Only fill in the external product note if it's empty and you have a value to fill it in.
                if(String.isBlank(qli.zCRM_External_Product_Note__c)){
                    String language = parentQuotes.get(qli.QuoteId).zCRM_Language__c;
                    Id accountId = parentQuotes.get(qli.QuoteId).AccountId;

                    qli.zCRM_External_Product_Note__c = getExternalProductNote(productSalesTexts, productUtilizationSalesTexts, qli.Product2Id, accountId, language);
                }
            }

        }

        public String getExternalProductNote(Map<Id, Map<String, String>> productSalesTexts, Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts, Id productId, Id accountId, String language){
            String pst = '';
            if(productSalesTexts.containsKey(productId)){
                if(productSalesTexts.get(productId).containsKey(language)){
                    pst = productSalesTexts.get(productId).get(language);
                }else if(productSalesTexts.get(productId).containsKey('EN')){
                    pst = productSalesTexts.get(productId).get('EN');
                }
            }

            String pust = '';
            if(productUtilizationSalesTexts.containsKey(productId)){
                if(productUtilizationSalesTexts.get(productId).containsKey(accountId)){
                    if(productUtilizationSalesTexts.get(productId).get(accountId).containsKey(language)){
                        pust = productUtilizationSalesTexts.get(productId).get(accountId).get(language);
                    }else if(productUtilizationSalesTexts.get(productId).get(accountId).containsKey('EN')){
                        pust = productUtilizationSalesTexts.get(productId).get(accountId).get('EN');
                    }
                }
            }

            return combinePstPust(pst, pust);
        }

        private String combinePstPust(String pst, String pust){
            String externalProductNote;
            if(String.isNotBlank(pst) && String.isNotBlank(pust)){
                externalProductNote = pst + '\n' + pust;
            }else if(String.isBlank(pst)){
                externalProductNote = pust;
            }else if(String.isBlank(pust)){
                externalProductNote = pst;
            }
            return externalProductNote;
        }

        // Accessed with ProductId then Language
        public Map<Id, Map<String, String>> getProductSalesTexts(Set<Id> productIds){
            Map<Id, Map<String, String>> dictionary = new Map<Id, Map<String, String>>();
            for(zCRM_Product_Sales_Text__c pst : [SELECT Id, zCRM_Language__c, zCRM_SAP_Sales_Text__c, zCRM_Product__c FROM zCRM_Product_Sales_Text__c WHERE zCRM_Product__c IN :productIds]){
                if(!dictionary.containsKey(pst.zCRM_Product__c)){
                    dictionary.put(pst.zCRM_Product__c, new Map<String, String>());
                }

                dictionary.get(pst.zCRM_Product__c).put(pst.zCRM_Language__c, pst.zCRM_SAP_Sales_Text__c);
            }
            return dictionary;
        }

        //Accessed with ProductId then AccountId then Language
        public Map<Id, Map<Id, Map<String, String>>> getProductUtilizationSalesTexts(Set<Id> productIds){
            Map<Id, Map<Id, Map<String, String>>> dictionary = new Map<Id, Map<Id, Map<String, String>>>();
            for(zCRM_Product_Utilization_Sales_Text__c pust : [SELECT Id, zCRM_Language__c, zCRM_SAP_Sales_Text__c, zCRM_Product_Utilization__r.zCRM_Product__c, zCRM_Product_Utilization__r.zCRM_Account__c FROM zCRM_Product_Utilization_Sales_Text__c WHERE zCRM_Product_Utilization__r.zCRM_Product__c IN :productIds]){
                if(!dictionary.containsKey(pust.zCRM_Product_Utilization__r.zCRM_Product__c)){
                    dictionary.put(pust.zCRM_Product_Utilization__r.zCRM_Product__c, new Map<Id, Map<String, String>>());
                }

                if(!dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).containsKey(pust.zCRM_Product_Utilization__r.zCRM_Account__c)){
                    dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).put(pust.zCRM_Product_Utilization__r.zCRM_Account__c, new Map<String, String>());
                }

                dictionary.get(pust.zCRM_Product_Utilization__r.zCRM_Product__c).get(pust.zCRM_Product_Utilization__r.zCRM_Account__c).put(pust.zCRM_Language__c, pust.zCRM_SAP_Sales_Text__c);
            }
            return dictionary;
        }

        public String buildExternalProductNote(QuoteLineItem quoteLineItem){
            Map<Id, Map<String, String>> productSalesTexts = getProductSalesTexts(new Set<Id>{quoteLineItem.Product2Id});
            Map<Id, Map<Id, Map<String, String>>> productUtilizationSalesTexts = getProductUtilizationSalesTexts(new Set<Id>{quoteLineItem.Product2Id});
            Quote parent = [SELECT zCRM_Language__c, AccountId FROM Quote WHERE Id = :quoteLineItem.QuoteId ];

            return getExternalProductNote(productSalesTexts, productUtilizationSalesTexts, quoteLineItem.Product2Id, parent.AccountId, parent.zCRM_Language__c);
        }

    }
    public class RemovePCMCsIfFieldsChanged implements Triggers.Handler {
        public void handle() {
            Set<String> fieldsToCheck = getFieldsToCheck();

            if(!fieldsToCheck.isEmpty()){
                Set<Id> changedQuoteLinesItemIds = new Set<Id>();
                Set<Id> quoteIdsToUpdate = new Set<Id>();

                for (QuoteLineItem newLine : (List<QuoteLineItem>)Trigger.new) {
                    QuoteLineItem oldLine = (QuoteLineItem)Trigger.oldMap.get(newLine.Id);

                    for(String field : fieldsToCheck){
                        if(newLine.get(field) != oldLine.get(field)){
                            changedQuoteLinesItemIds.add(newLine.Id);
                            quoteIdsToUpdate.add(newLine.QuoteId);
                            break;
                        }
                    }
                }
                if (!changedQuoteLinesItemIds.isEmpty()) {
                    //Delete Price Conditions and Metal Classifciations
                    deletePriceConditionsAndMetalClassifications(changedQuoteLinesItemIds);

                    //Update the quotes simulation status
                    changeQuoteSimulationStatusToResimulate(quoteIdsToUpdate);
                }
            }
        }
        public Set<String> getFieldsToCheck(){
            List<String> fieldsToCheck = new Utils.Pluck().strings(
                    zCRM_Lightning_Layout_Column__c.zCRM_SaveField__c,
                    new List<zCRM_Lightning_Layout_Column__c>([SELECT zCRM_SaveField__c FROM zCRM_Lightning_Layout_Column__c WHERE zCRM_Resets_PC_MC__c = TRUE]));
            return new Set<String>(fieldsToCheck);
        }
        public void deletePriceConditionsAndMetalClassifications(Set<Id> quoteLineItemIds){
            List<SObject> toDelete = new List<SObject>();
            toDelete.addAll((List<SObject>)[SELECT Id FROM zCRM_Quote_Line_Price_Condition__c WHERE zCRM_Quote_Line_Item__c IN :quoteLineItemIds]);
            toDelete.addAll((List<SObject>)[SELECT Id FROM zCRM_Quote_Line_Metal_Classification__c WHERE zCRM_Quote_Line_Item__c IN :quoteLineItemIds]);
            
            for(List<SObject> batch : Utils.createBatches(toDelete)){
                delete batch;
            }
        }
        public void changeQuoteSimulationStatusToResimulate(Set<Id> quoteIds){
            List<Quote> quotes = new List<Quote>([SELECT Id, zCRM_Simulation_Status__c FROM Quote WHERE Id IN :quoteIds]);
            for(Quote q : quotes) q.zCRM_Simulation_Status__c = 'To Resimulate';
            
            for(List<SObject> batch : Utils.createBatches(quotes)){
                update batch;
            }
        }
    }
	public class RollupLineComments implements Triggers.Handler {
        public String buildInternalComments(Quote q){
            return String.isBlank(q.zCRM_Internal_Comments__c)
                    ? ''
                    : q.zCRM_Internal_Comments__c.substringBefore('\nLINE COMMENTS');
        }
        public String buildInternalComments(QuoteLineItem qli){
            return String.isBlank(qli.zCRM_Internal_Comments__c) || qli.zCRM_Won_Lost__c != 'Won'
                    ? ''
                    : qli.zCRM_Product_Name__c + '-' + qli.zCRM_Internal_Comments__c;
        }

	    public void handle() {
            //Filter out the QuoteLineItems where the zCRM_Internal_Comments__c field was changed
            List<QuoteLineItem> changedQuoteLineItems = new Utils.Filter().changedField(QuoteLineItem.zCRM_Internal_Comments__c, Trigger.new, Trigger.old);
            if(changedQuoteLineItems.isEmpty()) return;

//            Still in the codebase to demonstrate how above filter can be used.
//            List<QuoteLineItem> filtered = new List<QuoteLineItem>();
//            for(QuoteLineItem qli : (List<QuoteLineItem>)Trigger.new){
//                if(qli.zCRM_Internal_Comments__c != ((QuoteLineItem)Trigger.oldMap.get(qli.Id)).zCRM_Internal_Comments__c)
//                    filtered.add(qli);
//            }


            Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId = new Utils.GroupBy().ids(QuoteLineItem.QuoteId, changedQuoteLineItems);

            List<Quote> quotesToUpdate = new List<Quote>();

		    for (Quote qte : [SELECT Id, zCRM_Internal_Comments__c FROM Quote WHERE Id IN :quoteLineItemsByQuoteId.keySet() AND Status = 'In The Bag']) {
                String quoteInternalComments = buildInternalComments(qte);

                List<String> qliInternalComments = new List<String>();
                for(QuoteLineItem quoteLineItem : quoteLineItemsByQuoteId.get(qte.Id)){
                    String qliInternalComment = buildInternalComments(quoteLineItem);

                    if(!String.isBlank(qliInternalComment)) //null or ''
                        qliInternalComments.add(qliInternalComment);
                }

                quoteInternalComments = (String.isBlank(quoteInternalComments) ? '' : quoteInternalComments + '\n') +
                    (qliInternalComments.isEmpty() ? '' : 'LINE COMMENTS:\n' +
							String.join(qliInternalComments, '\n'));


                if(!qte.zCRM_Internal_Comments__c.equals(quoteInternalComments)){
			    	qte.zCRM_Internal_Comments__c = quoteInternalComments;
					quotesToUpdate.add(qte);
                }

            }

            if(!quotesToUpdate.isEmpty())
                update quotesToUpdate;
	    }
    }
}