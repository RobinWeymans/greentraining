trigger QuoteLineItemTrigger on QuoteLineItem (before update, before insert, before delete ) {
    new Triggers()
            //Pre release 2
            .bind(Triggers.Evt.beforeupdate, new QuoteLineItemTriggerHandler.LegacyBeforeUpdate())
            .bind(Triggers.Evt.beforeinsert, new QuoteLineItemTriggerHandler.LegacyBeforeInsert())
            .bind(Triggers.Evt.beforedelete, new QuoteLineItemTriggerHandler.LegacyBeforeDelete())

            //Release 2
            .bind(Triggers.Evt.beforeinsert, new QuoteLineItemTriggerHandler.AutoFillExternalProductNote())
            .bind(Triggers.Evt.beforeupdate, new QuoteLineItemTriggerHandler.RemovePCMCsIfFieldsChanged())
		    .bind(Triggers.Evt.beforeupdate, new QuoteLineItemTriggerHandler.RollupLineComments())
		    
            .manage();
}