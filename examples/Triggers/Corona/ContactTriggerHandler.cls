/**
 * @date 2015-03-25
 * @author <a href="mailto:andries.neyens@4cconsulting.com?Subject=ContactTriggerHandler">Andries Neyens</a>
 * <p><i>4C Consulting, Stationsstraat 60/5 2800 Mechelen</i>
 * @description Handler code of the Contact Trigger. <p>
 *              We are using the TriggerHandler Framework (https://github.com/kevinohara80/sfdc-trigger-framework/tree/master/src#)
 * BeforeUpdate
 *   - FixStreetAndMatchCodeAddress
 *   - linkAccountsToContacts
 *   - CorrectParameterCode
 *   - FillInLogo
 *   - LinktoEmail
 *
 * BeforeInsert
 *   - FixStreetAndMatchCodeAddress
 *   - linkAccountsToContacts
 *   - CorrectParameterCode
 *   - FillInLogo
 *   - LinktoEmail
 *   
 * @see SYS_ParameterCodeService.CorrectParameterCode
 */
public without sharing class ContactTriggerHandler extends TriggerHandler {

    /**
    * beforeUpdate handler override of the Contact Object
    *   - FixStreetAndMatchCodeAddress
    *   - linkAccountsToContacts
    *   - CorrectParameterCode
    *   - FillInLogo
    *   - LinktoEmail
    * @return void
    * @see SYS_ParameterCodeService.CorrectParameterCode
    */
    public override void beforeUpdate() {
        FixStreetAndMatchCodeAddress((List<Contact>) Trigger.new);
        FixMarketingAutomationFields((List<Contact>) Trigger.new);
        FixMobilePhone((List<Contact>) Trigger.new);
        linkAccountsToContacts((List<Contact>) Trigger.new);
        SYS_ParameterCodeService.GetInstance.CorrectParameterCode(Trigger.new);  
        FillInLogo( (List<Contact>)Trigger.new, (Map<Id,Contact>) Trigger.oldMap);  
        LinktoEmail((List<Contact>) Trigger.new, (Map<Id, Contact>) Trigger.oldMap, Trigger.isInsert);
        corlib_Individual_Service.AttachIndividual(Trigger.new, Trigger.isInsert);
        SyncToBO( (List<Contact>)Trigger.new, (Map<Id, Contact>)Trigger.oldMap);
    }

    /**
    * beforeInsert handler override of the Contact Object
    *   - FixStreetAndMatchCodeAddress
    *   - linkAccountsToContacts
    *   - CorrectParameterCode
    *   - FillInLogo
    *   - LinktoEmail
    * @return void
    * @see SYS_ParameterCodeService.CorrectParameterCode
    */
    public override void beforeInsert() {
        FixStreetAndMatchCodeAddress((List<Contact>) Trigger.new);
        FixMarketingAutomationFields((List<Contact>) Trigger.new);
        // FixMobilePhone((List<Contact>) Trigger.new);
		linkAccountsToContacts((List<Contact>) Trigger.new);
		// SYS_ParameterCodeService.GetInstance.CorrectParameterCode(Trigger.new);    
        FillInLogo( (List<Contact>)Trigger.new, null);
        LinktoEmail((List<Contact>) Trigger.new, (Map<Id, Contact>) Trigger.oldMap, Trigger.isInsert);
        corlib_Individual_Service.AttachIndividual(Trigger.new, Trigger.isInsert);
    }
  
  

    @TestVisible
	private static void FixMobilePhone(List<Contact> contacts){
		for(Contact c: contacts){
			if (String.isNotBlank(c.MobilePhone)){
                c.MobilePhone_E164__c = c.MobilePhone.replaceAll('[\\.\\/\\s]', '');
                if (c.MobilePhone_E164__c.startsWith('0')) c.MobilePhone_E164__c = '+32' + c.MobilePhone_E164__c.Right(c.MobilePhone_E164__c.length() - 1);
			}
        }
	}



	@TestVisible
	/**
	 * Try to sanitize the MA fields: email is only valid if there is an email, sms only if there is a mobile
	 * @param contacts a List of Contacts, from the Trigger.new Context
	 */
	private void FixMarketingAutomationFields(List<Contact> contacts) {
		for(Contact c: contacts) {
            if (c.MA_IsDigitalCommunicationAllowed__c && String.isBlank(c.Email)) c.MA_IsDigitalCommunicationAllowed__c = false;
            if (c.MA_IsSMSReminderAllowed__c  && String.isBlank(c.MobilePhone)) c.MA_IsSMSReminderAllowed__c = false;
        }
    }


    @TestVisible
	private static void SyncToBO(List<Contact> contacts, Map<Id, Contact> oldContacts){
		
		Set<Id> contactsToSendOut = new Set<Id>();

		for(Contact c: contacts){
			if (c.Sync_ToBO__c && ( oldContacts.get(c.id).MobilePhone != c.MobilePhone || 
                                      oldContacts.get(c.id).Email != c.Email ||  
                                      oldContacts.get(c.id).MA_IsDigitalCommunicationAllowed__c != c.MA_IsDigitalCommunicationAllowed__c ||
                                      oldContacts.get(c.id).MA_IsSMSReminderAllowed__c != c.MA_IsSMSReminderAllowed__c )){
				contactsToSendOut.add(c.Id);
			}
		}

		if (contactsToSendOut.size() > 0) SendToBO(contactsToSendOut);
	}

	@future(callout=true)
	private static void SendToBO(Set<Id> ids){
		if (ids == null || ids.size()==0) return;	//bail out

		try {
			List<Contact> contacts = corlib_Contact_Selector.selectById(ids);

			REST rest = new REST(corlib_Configuration.BO_Endpoint() + '/v3.3/customer/update/preference', 'POST');
			
			List<Map<String,Object>> data = new List<Map<String,Object>>();
			for (Contact c: contacts){
				Map<String,Object> jsonData = new Map<String,Object>();
				jsonData.put('personNr', c.BO_PERSOON_NR__c);
				jsonData.put('mobile', c.MobilePhone);
				jsonData.put('email', c.Email);
				jsonData.put('digitalCommunication', c.MA_IsDigitalCommunicationAllowed__c);
				jsonData.put('smsReminder', c.MA_IsSMSReminderAllowed__c);

				data.add(jsonData);
			}

			string j = rest.Send(new Map<String,String>{'body' => JSON.serialize(data)});
            /*
            http://accapi.corona.be/restservices/v3.3/customer/update/preference --> POST
            [{
            "personNr":"398569",
            "mobile":"011999999",
            "email":"jan.pannenkoek@coronadirect.be",
            "digitalCommunication":false,
            "smsReminder":false
            },
            {
            "personNr":"999999",
            "mobile":"012999999",
            "email":"john.doe@coronadirect.be",
            "digitalCommunication":true,
            "smsReminder":true
            }
            ]
            */


        } catch(Exception e){
            Logger.Error(e);
        } finally {
            //making sure everything is written to the DB
            Logger.Flush();
        }

	}

  @TestVisible
  /**
   * 
   * @param contacts       List of Contacts to adapt
   * @param oldContactsMap List of previous contacts from the trigger.old map
   */
  private static void FillInLogo(List<Contact> contacts, Map<Id,Contact> oldContactsMap) {

    List<Contact> contactsWithNewLPartnerLabels = new List<Contact>(); List<String> partnerLabels = new List<String>();
    for(Contact p: contacts){
      //find all changes
      if ((oldContactsMap == null )  ||                                    //insert
          (oldContactsMap != null  && p.PartnerLabel__c != oldContactsMap.get(p.Id).PartnerLabel__c ) ||    //update
         (String.isNotBlank(p.PartnerLabel__c) && String.isBlank(p.Logo__c))){                  //Legacy
            contactsWithNewLPartnerLabels.add(p);
            partnerLabels.add(p.PartnerLabel__c);
      }
    }

    // find all logo's and put them in the policy
    List<String> logos = corlib_Logo.GetLogos(partnerLabels);
    for (Integer i = 0; i < contactsWithNewLPartnerLabels.size(); i++) {
      contactsWithNewLPartnerLabels[i].Logo__c = logos[i];
    }
  }

    /**
    * Tries to find an Email in the SF database with the provided contact email or creates a new one
    * Links the email to the email__r lookup
    * @param  newList  Trigger.new    
    * @param  oldMap  Trigger.oldMap  
    * @param  isInsert  Trigger.isInsert  
    */
    private static void LinktoEmail(List<Contact> newList, Map<id, Contact> oldMap, Boolean isInsert){
        
        List<Contact> contactsToUpdate = new List<Contact>();
        Set<String> keysetFromContacts = new Set<String>();
        Map<String, Email__c> existingEmailRecordsMap = new Map<String, Email__c>();
        Map<String, Email__c> newEmailRecordsMap = new Map<String, Email__c>();
        
        //Generate keys to check if Email__c with this key already exists
        for(Contact c : newList){
            if ((isInsert && String.isNotBlank(c.Email) ) ||                                                //insert
                (!isInsert && String.isNotBlank(c.Email) && c.Email != oldMap.get(c.id).Email) ||           //update
                (!isInsert && String.isNotBlank(c.Email) && c.Email__c == null)){                           //legacy
                    contactsToUpdate.add(c);
                    keysetFromContacts.add(corlib_Email_Service.GenerateKey(c.Logo__c, c.Email));
                }
            else if(!isInsert && String.isBlank(c.Email) && String.isNotBlank(oldMap.get(c.id).Email)){
                c.Email__c = null;
            }
        }
        
        if(keysetFromContacts.size()>0){
            //retrieve list of Email__c that already exists and put them in existingEmailRecordsMap
            List<Email__c> emailRecords = corlib_Email_Selector.selectByKey(keysetFromContacts);
            if( emailRecords != null && emailRecords.size() > 0 ){
                for(Email__c e : emailRecords){
                    existingEmailRecordsMap.put(e.EmailKey__c , e);
                }
            }
        }
        
        for(Contact c : contactsToUpdate){
            String contactKey = corlib_Email_Service.GenerateKey(c.Logo__c, c.Email);
            if(!existingEmailRecordsMap.containsKey(contactKey) ){
                Email__c newEmail = corlib_Email_Service.GetNewRecord();
                newEmail.LanguageCode__c = c.LanguageCode__c;
                newEmail.Email__c = c.Email;
                if(String.isNotBlank(c.Logo__c)) newEmail.Logo__c = c.Logo__c;
                newEmailRecordsMap.put(contactKey,newEmail);
            }
        }

        corlib_Email_Service.Save(newEmailRecordsMap.values());        
        
        for (Contact c: contactsToUpdate){
            String contactKey = corlib_Email_Service.GenerateKey(c.Logo__c, c.Email);
            if(newEmailRecordsMap.containsKey(contactKey)){
                c.Email__c = newEmailRecordsMap.get(contactKey).Id;
            }else if(existingEmailRecordsMap.containsKey(contactKey)){
                c.Email__c = existingEmailRecordsMap.get(contactKey).Id;
            }
            
        }
    }

	@TestVisible
	/**
	 * Insert dummy household accounts for contacts.
	 * @param contacts a List of Contacts, from the Trigger.new Context
	 */
	private void linkAccountsToContacts(List<Contact> contacts) {
		List<Account> insertedAccounts = new List <Account>();

		for(Contact c: contacts) {
          if (c.AccountId == null){
                   insertedAccounts.add(
                  new Account( 
                            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId(),
                                                  Name = (String.isNotBlank(c.LastName)?c.LastName.toUpperCase():null),
                                                  BillingStreetName__c = c.MailingStreetName__c,
                                                  BillingHouseNumber__c = c.MailingHouseNumber__c,
                                                  BillingBoxNumber__c = c.MailingBoxNumber__c,
                                                  BillingPostalCode = c.MailingPostalCode,
                                                  BillingCity = c.MailingCity,
                                                  BillingStreetRoad65ID__c = c.MailingStreetRoad65ID__c,
                                                  OwnerId = corlib_Configuration.Account_Owner()
                    ));			
          }
        }

        if (insertedAccounts.size() >0){
          insert insertedAccounts;
    
          Integer i = 0;
          for(Contact c: contacts) {
            if (c.AccountId == null){
              c.AccountId = insertedAccounts[i].Id;
              i++;
            }
          }
        }
    }

    /**
	 * Concatinates the MailingStreet with the custom fields MailingHouseNumber__c and MailingBusNumber__c.
	 * it also calculates the External ID MatchCodeAddress__c with the function GetMatchCodeAddress
	 * @param cons List of Contacts to fix the MailingStreet on
	 * @see GetMatchCodeAddress
	 */
	private void FixStreetAndMatchCodeAddress(List<Contact> cons){

		for (Contact c: cons) {   
			c.MatchCodeAddress__c = GetMatchCodeAddress(c.MailingStreetRoad65ID__c, c.MailingHousenumber__c, c.MailingBoxNumber__c);	 

			c.MailingStreet = String.isBlank(c.MailingStreetName__c)?'':c.MailingStreetName__c;
			if (String.IsNotBlank(c.MailingHouseNumber__c)) c.MailingStreet = c.MailingStreet + ' ' + c.MailingHouseNumber__c;
			if (String.IsNotBlank(c.MailingBoxNumber__c)) c.MailingStreet = c.MailingStreet + ' ' + c.MailingBoxNumber__c;		
		
			if(c.Birthdate != null){
				c.v_Birthdate__c = String.valueOf(c.Birthdate.Day()) + String.valueOf(c.Birthdate.Month()) + String.valueOf(c.Birthdate.Year());
			}
		}

	}

    /**
     * Generates a unique key on the provided address parameters. This key can be used as external id.
     * @param  road65Id    decimal with the external id of road65 
     * @param  houseNumber the house number
     * @param  busNumber   the busnumber, if there is one
     * @return             a unique concat that can be used as an external id for that specific address.
     * @complexity  O(1), beause of the use of indexes
     */
    public static string GetMatchCodeAddress(Decimal road65Id, String houseNumber, String busNumber){
        return String.ValueOf(road65Id==null?'':String.ValueOf(road65Id.intValue())).toLowerCase() + '_' + String.ValueOf(String.isBlank(houseNumber)?'':houseNumber).toLowerCase() + String.ValueOf(String.isBlank(busNumber)?'':'_' + busNumber).toLowerCase();
    }
}