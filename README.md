# Training Sander & Gerard

## Introduction
Apex Enterprise Patterns: Service layer

_Defines an application's boundary with a layer of services that establishes a set of available operations and coordinates the application's response in each operation._

The Service layer helps you form a clear and strict encapsulation of code implementing business tasks, calculations and processes. It’s important to ensure that the Service layer is ready for use in different contexts, such as mobile applications, UI forms, rich web UIs, and numerous APIs. It must remain pure and abstract to endure the changing times and demands ahead of it. The following sections define the guidelines for creating a Service layer implementation in Apex while also keeping in mind Salesforce best practices and governor limits.

https://trailhead.salesforce.com/en/content/learn/modules/apex_patterns_sl



## Install SFDX CLI
In this demo, you will be using Salesforce DX. You can download and install SFDX CLI here: 

https://developer.salesforce.com/tools/sfdxcli

## Install VSCode DX plugins
You can install SFDX VS Code plugins by executing (Ctrl+P). The plugins are essentially a wrapper around the sfdx cli so that it can be used interactively. To see all different actions, press Ctrl+Shift+P.

```ext install salesforce.salesforcedx-vscode```

## Configure SFDX
Make a free trial account here:
https://developer.salesforce.com/promotions/orgs/dx-signup

Connect your local cli with new DevHub account
```
sfdx force:auth:web:login -d -a TrialDevHub
```

## Git
Make a new folder for your project and clone the following repository: https://RobinWeymans@bitbucket.org/RobinWeymans/bami.git. 

Its a simplified version of https://github.com/sfdx-isv/sfdx-falcon-template
```
mkdir bami
cd bami
git clone https://RobinWeymans@bitbucket.org/RobinWeymans/bami.git .
git checkout 8769a336e6fc12f007631bb039c4ec979e5d429a
rm -rf .git
```
Initialise your own git repository
```
git init
git commit --allow-empty -m "Initial commit"
git add * .*
git commit -m "Added falcon template"
```

You can use whatever remote you want/have. In my case:
```
git remote add origin https://RobinWeymans@bitbucket.org/RobinWeymans/bami.git
git push --set-upstream origin master
```

## Scratch org
Create a scratch org by using the sfdx plugin or with the following command
```
sfdx force:org:create -f config/project-scratch-def.json -a Bami -s
```

# Deploying to Scratch Orgs
We are going to add 2 lightweight frameworks to our codebase.
1. TriggerHandler Framework
2. TestFactory Framework

Create a few new folders
```
cd sfdx-source/bami
mkdir lib
mkdir lib/TestFactory
mkdir lib/TestFactory/classes
mkdir lib/TriggerHandler
mkdir lib/TriggerHandler/classes
```
Create the following classes and copy the code from res/
- TestFactory
- TestFactoryDefaults
- TestFactoryTest
- Triggers
- TriggersTest

Push the code to your (default) scratch org
```
sfdx force:source:push
```

You should see something like this:
```
=== Pushed Source
STATE  FULL NAME            TYPE       PROJECT PATH
─────  ───────────────────  ─────────  ─────────────────────────────────────────────────────────────────────────
Add    TestFactory          ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactory.cls
Add    TestFactory          ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactory.cls-meta.xml
Add    TestFactoryDefaults  ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactoryDefaults.cls
Add    TestFactoryDefaults  ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactoryDefaults.cls-meta.xml
Add    TestFactoryTest      ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactoryTest.cls
Add    TestFactoryTest      ApexClass  sfdx-source/bami/lib/TestFactory/classes/TestFactoryTest.cls-meta.xml
```

Your Apex classes are now available in your scratch org
```
sfdx force:org:open
```

## Configuration in Salesforce
Its also possible to configure some stuff in Salesforce and retrieve it into your local codebase.

Create a new object called Boat and retrieve the new object.
```
sfdx force:source:pull
```

Move the retrieved files to folders so that it fits your project strucure, for example:
- sfdx-source
  - bami
    - Boaryard
      - layouts
    - Datamodel
      - objects
    - lib
      - TestFactory
        - classes
      - Triggers
        - classes
  - unpackaged
    - profiles

## Deploying code to Sandboxes with DX
If you have an extra dev org, you can link it like so:
```
sfdx force:auth:web:login -a <Alias> -r https://test.salesforce.com
```


There are two options to deploy code to a sandbox org:
- Meta data api
  1. Convert codebase to mdapi format
  ```
  sfdx force:source:convert -r sfdx-source/bami --outputdir ./mdapi-source
  sfdx force:source:convert -r sfdx-source/bami/Boatyard --outputdir ./mdapi-source
  ```
  2. Deploy the code
  ```
  sfdx force:mdapi:deploy -d ./mdapi-source -u <sandbox-alias> -w 15 -l RunLocalTests
  ```
- DX api
```
sfdx force:source:deploy -u <sandbox-alias> -p sfdx-source/bami
sfdx force:source:deploy -u <sandbox-alias> -m ApexClass:Triggers
sfdx force:source:deploy -u <sandbox-alias> -m "ApexClass,ApexTrigger,Layout"
```

You can also deploy code by using the workbench (not advised). This uses the metadata API.

## Deploying code to Production orgs with DX
Deploying code to a production org is only possible through the metadata api, because you need to run tests.


# Creating a simple Logging Application
Create a new Custom object Log__c in Salesforce setup, also create a tab

Create picklist field Level__c with values DEBUG, INFO, WARN, ERROR

Create long text field Message__c

Retrieve all the metadata using DX

Create new lightning application called Logger, and add nothing but the Log__c tab

Create a Logger class that does basic logging. You can copy res/Logger if you really want.
```
public static void debug(String msg);
public static void info(String msg);
public static void warn(String msg);
public static void error(String msg);
public static void flush();
```

Make a new file in folder ```scripts``` with contents
```
Logger.debug('Special message');
Logger.flush();
```

Execute and debug the anonymous apex in VS code

# Deploying all apps to shared org
Deploy the code in the Logger directory to your personal DEV org