public with sharing class Logger {

    private static List<Log__c> logs = new List<Log__c>();
    
    public static void debug(String msg){
        log(System.LoggingLevel.DEBUG, msg);
    }

    public static void info(String msg){
        log(System.LoggingLevel.INFO, msg);
    }

    public static void warn(String msg){
        log(System.LoggingLevel.WARN, msg);
    }

    public static void error(String msg){
        log(System.LoggingLevel.ERROR, msg);
    }

    private static void log(System.LoggingLevel level, String msg){
        System.debug(level, msg);
        logs.add(new Log__c(
            Level__c = String.valueOf(level),
            Message__c = msg
        ));
    }

    public static void flush(){
        insert logs;
        logs.clear();
    }

}
