public class Triggers {
    /**
      *	@description Enum representing each of before/after CRUD events on Sobjects
      */
    public enum Evt {
        afterdelete, afterinsert, afterundelete,
        afterupdate, beforedelete, beforeinsert, beforeupdate
    }

    /**
      * @description Simplistic handler to implement on any of the event. It doesn't requires or enforces any patter except the
        method name to be "handle()", a developer is free to use any Trigger context variable or reuse any other
        apex class here.
    */
    public interface Handler {
        void handle();
    }

    /** Internal mapping of handlers */
    private Map<String, List<Handler>> eventHandlerMapping = new Map<String, List<Handler>>();

    /**
        Core API to bind handlers with events
    */
    public Triggers bind(Evt event, Handler eh) {
        List<Handler> handlers = eventHandlerMapping.get(event.name());
        if (handlers == null) {
            handlers = new List<Handler>();
            eventHandlerMapping.put(event.name(), handlers);
        }
        handlers.add(eh);
        return this;
    }

    /**
        Invokes correct handlers as per the context of trigger and available registered handlers
    */
    public void manage() {
        Evt ev = null;

        if((Trigger.isExecuting && Trigger.isInsert && Trigger.isBefore) || TriggersTest.isBeforeInsertForced) {
            ev = Evt.beforeinsert;
        } else if((Trigger.isExecuting && Trigger.isInsert && Trigger.isAfter) || TriggersTest.isAfterInsertForced){
            ev = Evt.afterinsert;
        } else if((Trigger.isExecuting && Trigger.isUpdate && Trigger.isBefore) || TriggersTest.isBeforeUpdateForced){
            ev = Evt.beforeupdate;
        } else if((Trigger.isExecuting && Trigger.isUpdate && Trigger.isAfter) || TriggersTest.isAfterUpdateForced){
            ev = Evt.afterupdate;
        } else if((Trigger.isExecuting && Trigger.isDelete && Trigger.isBefore) || TriggersTest.isBeforeDeleteForced) {
            ev = Evt.beforedelete;
        } else if((Trigger.isExecuting && Trigger.isDelete && Trigger.isAfter) || TriggersTest.isAfterDeleteForced){
            ev = Evt.afterdelete;
        } else if((Trigger.isExecuting && Trigger.isundelete) || TriggersTest.isUndeleteForced){
            ev = Evt.afterundelete;
        }
        List<Handler> handlers = eventHandlerMapping.get(ev.name());
        if (handlers != null && !handlers.isEmpty()) {
            for (Handler h : handlers) {
                h.handle();
            }
        }
    }
}